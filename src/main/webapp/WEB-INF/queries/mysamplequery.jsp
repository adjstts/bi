<%@ page session="true" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.tonbeller.com/jpivot" prefix="jp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<%-- http://jpivot.sourceforge.net/index.html --%>
<%-- https://jdbc.postgresql.org/download.html --%>
<%-- https://jdbc.postgresql.org/documentation/head/connect.html --%>
<%-- https://jdbc.postgresql.org/documentation/head/load.html --%>
<jp:mondrianQuery id="query01" jdbcDriver="org.postgresql.Driver" jdbcUrl="jdbc:postgresql://localhost/jpivot?user=jpivot&password=jpivot" catalogUri="/WEB-INF/queries/MySampleSchema.xml">
  select
    {[Measures].[Count], [Measures].[Revenue]} on columns,
    {[Service Code].[All Service Codes]} on rows
  from [Services]
</jp:mondrianQuery>

<c:set var="title01" scope="session">My Sample Query</c:set>
