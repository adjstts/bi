# Business Intelligence

Изучение бизнес-аналитики и её инструментов

## Mondrian
Сервер бизнес-аналитики Mondrian используется для отображения модели OLAP-кубов
на таблицы реляционнной базы данных и выполнения поступающих от клиента
MDX-запросов против этих OLAP-кубов.

### Ресурсы
* https://mondrian.pentaho.com/documentation/olap.php -- документация Mondrian 3. Здесь есть всё необходимое для начала рабты с Mondrian.
* https://sourceforge.net/projects/mondrian/files/mondrian -- дистрибутивы Mondrian.
* https://github.com/pentaho/mondrian -- github Mondrian. Из документации на него ссылок нет.
* https://community.hitachivantara.com/s/article/mondrian -- статья про Mondrian на сайте Hitachi. Тут есть ссылка на Mondrian 3.14.0.

### Заметки
#### Отсутствующий дистрибутив со встроенной БД
После версии Mondrian 3.5.0 [на Sourceforge](https://sourceforge.net/projects/mondrian/files/mondrian)
из набора дистрибутивов в релизах пропал дистрибутив со встроенной БД Derby (mondrian-`<version>`-derby.zip).
При чём в файлике RELEASE.txt для следующих релизоввсё ещё продолжают писать, что
в релиз включём дистрибутив со встроенной БД Derby, несмотря на то, что это
откровенная ложь и не совсем так.

#### Плавающий формат дистрибутива
После версии Mondrian 3.7.0 похоже сменился формат дистрибуции, потому что
до этой версии включительно дистрибутивы [на Sourceforge](https://sourceforge.net/projects/mondrian/files/mondrian)
являлись zip-файлами, на которые есть отсылки в [документацтии по установке Mondrian](https://mondrian.pentaho.com/documentation/installation.php),
а после версии 3.7.0 [на Sourceforge](https://sourceforge.net/projects/mondrian/files/mondrian) дистрибутивы являются некоторыми jar-архивами.
Причём содержимое этих jar-архивов отличается от содержимого zip-архивов.

#### Release Notes
**В zip-дистрибутивах Mondrian есть очень полезный для прочтения файлик RELEASE.txt.
Его обязательно надо читать, чтобы погрузиться в тему.**

#### Невоспроизводимая инструкция пол импорту тестовых данных
Пытался, следуя [документации по установке Mondrian](https://mondrian.pentaho.com/documentation/installation.php)
выполнить импорт Mondrian Food Mart датасета и у меня конечно ничего не получилось:
```
adjstts@vlad-desktop:~/Desktop$ java -cp "/home/adjstts/Desktop/mondrian-sandbox/mondrian/lib/mondrian.jar:/home/adjstts/Desktop/mondrian-sandbox/mondrian/lib/log4j.jar:/home/adjstts/Desktop/mondrian-sandbox/mondrian/lib/commons-logging.jar:/home/adjstts/Desktop/mondrian-sandbox/mondrian/lib/eigenbase-xom.jar:/home/adjstts/Desktop/mondrian-sandbox/mondrian/lib/eigenbase-resgen.jar:/home/adjstts/Desktop/mondrian-sandbox/mondrian/lib/eigenbase-properties.jar:/home/adjstts/Desktop/mondrian-sandbox/postgresql-jdbc.jar" mondrian.test.loader.MondrianFoodMartLoader -verbose -tables -data -indexes -jdbcDrivers=org.postgresql.Driver -inputFile=FoodMartCreateData.sql -outputJdbcURL="jdbc:postgresql://localhost/mondrian_food_mart?user=mondrian_sandbox&password=mondrian_food_mart"
Exception in thread "main" java.lang.NoClassDefFoundError: org/olap4j/mdx/Quoting
	at mondrian.test.loader.MondrianFoodMartLoader.<clinit>(MondrianFoodMartLoader.java:99)
Caused by: java.lang.ClassNotFoundException: org.olap4j.mdx.Quoting
	at java.net.URLClassLoader.findClass(URLClassLoader.java:381)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:424)
	at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:335)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:357)
	... 1 more
```

#### Невоспроизводимая инструкция по запуску веб-приложения (Mondrian и JPivot)
В разделе **3. Deploy and run the web application with a non-embedded database**
[документации по установке Mondrian](https://mondrian.pentaho.com/documentation/installation.php) сказано:
>2. From the unzipped binary release, explode lib/mondrian.war to TOMCAT_HOME/webapps/mondrian

Вся суть в том, что в zip-дистрибутиве нет никакого `lib/mondrian.war`. Там есть
`lib/mondrian.jar`, но является ли он веб-архивом, о котором идет речь в
документации -- неизвестно.

### Прочее
https://community.hitachivantara.com/s/article/data-integration-kettle -- Статья
на сайте Hitachi про Pentaho Data Integration Kettle. Это инструмент для ETL.
Надо разобраться, попробовать.

### Вывод
На [сайте с документацией Mondrian](https://mondrian.pentaho.com/documentation)
очень грустно всё с разделом про установку. Настолько грустно, что пользуясь
инструкцией по установке невозможно начать пользоваться Mondrian. Придётся пока
что начать использовать Mondrian через [JPivot](http://jpivot.sourceforge.net/).
Как с ним освоюсь, попробую без документации сам запустить чистый Mondrian (раз
по документации это невозможно).

## JPivot
JPivot -- это OLAP-клиент, рисующий Pivot-таблицы. Он под капотом использует
Mondrian (видимо, in-process). Надо разобраться с JPivot, попробовать запустить,
поразрабатывать чего-нибудь. Если получится, можно будет вернуться к документации
Mondrian и переосмыслить её. Может быть получится самому запустить чистый Mondrian...

### Ресурсы
* http://jpivot.sourceforge.net -- Документация по JPivot.
* https://sourceforge.net/projects/jpivot/files/ -- Дистрибутивы JPivot

### JSP (JavaServer Pages)
Пользовательский интерфейс в JPivot рисуется с помощью JSP. Поэтому, для
полноценного понимания работы JPivot, нужно разобраться и с JSP
[(JAVA-26 JSP)](https://gitlab.com/adjstts/java/issues/28). 