-- Чтобы запустить этот пример нужно поднять локальный сервер PostgreSQL, запустить psql против него и выполнить следующие команды:

-- Создаём пользователя jpivot и базу данных для него.
create user jpivot password 'jpivot';
create database jpivot owner jpivot;

-- Соединяемся с базой данных.
\c jpivot;

-- Создаём таблицу иерархии кодов услуг.
create table service_code(
  service_code_id bigint primary key,
  service_group text,
  service_code text);
alter table service_code owner to jpivot;

-- Заполняем таблицу иерархии кодов услуг тестовыми данными.
insert into service_code(service_code_id, service_group, service_code) values
  (1, '1А1', '1А1001'),
  (2, '1А1', '1А1002'),
  (3, '1А1', '1А1003'),
  (4, '1А3', '1А3001'),
  (5, '1А3', '1А3002'),
  (6, '1А3', '1А3003');

-- Создаём таблицу фактов выполненных услуг.
create table service_fact(
  service_fact_id bigint primary key,
  service_code_id bigint references service_code(service_code_id),
  count bigint,
  revenue numeric);
alter table service_fact owner to jpivot;

-- Заполняем таблицу фактов выполненных услуг тестовыми данными.
insert into service_fact(service_fact_id, service_code_id, count, revenue) values
  (1, 1, 24, 35007.23),
  (2, 2, 108, 543563.43),
  (3, 3, 12, 43563.76),
  (4, 4, 76, 74334.63),
  (5, 5, 532, 257743.58),
  (6, 6, 329, 25487656.29);
